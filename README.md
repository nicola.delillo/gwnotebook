

# GWnotebook
A naked collection of utils for gravitational waves data analysis.
Detailed descrprition: [GWnotebook](https://git.ligo.org/nicola.delillo/gwnotebook/blob/master/gwnotebook/__init__.py)

##  Dependencies

See [`requirements.txt`](https://git.ligo.org/nicola.delillo/gwnotebook/blob/master/requirements.txt)

## Installation
Download 

```bash
$ git clone git@git.ligo.org:nicola.delillo/gwnotebook.git
```

and install

```bash
$ python setup.py install
```


## Issue Tracker
if you spot some errors or  need more feature:
[https://git.ligo.org/nicola.delillo/gwnotebook/issues](https://git.ligo.org/nicola.delillo/gwnotebook/issues)

For contact nicola.delillo@ligo.org
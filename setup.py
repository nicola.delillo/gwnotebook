import setuptools
from setuptools import setup, find_packages

with open("README.md", "r") as fh:
    long_description = fh.read()

setup(
    name="gwnotebook", # Replace with your own username
    version="2020.2",
    author="Nicola De Lillo",
    author_email="nicola.delillo@ligo.org",
    description="A naked collection of utils for gravitational waves data analysis",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://git.ligo.org/nicola.delillo/gwnotebook",
    download_url='https://git.ligo.org/lscsoft/pesummary',
    packages=setuptools.find_packages(),
     install_requires=[
         'astropy>=3.2.3',
         'corner',
         'gwpy',
         'h5py',
         'healpy',
         'lalsuite',
         'matplotlib',
         'numpy>=1.15.4',
         'pandas<=0.24.2',
         'plotly',
         'scipy',
         'seaborn',
         'statsmodels',
         'tables',
         'tqdm'],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    license='MIT',
    python_requires='>=3.6',
    package_data={
        # If any package "package_name" contains *.txt or *.rst files, include them:
        "": ["*.txt", "*.rst"]
    }
)

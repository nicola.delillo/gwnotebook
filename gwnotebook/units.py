import astropy.units as u
# https://docs.astropy.org/en/stable/units/

Mpc = u.Mpc


# conversions
def rad_to_arcsec(rad):
    """
    Convert radians to arcsec 1'' = (180*3600)/π
    """
    arcsec = rad*3600*180/np.pi

    return arcsec

def arcsec_to_rad(arcsec):
    """
    Calculates parallax from radians to arcsec 1rad = 1" × π/(180 × 3600)
    """
    rad = arcsec/3600/180*np.pi

    return rad

from __future__ import division                                                  
import numpy as np                                                               
import matplotlib as mpl                                                         
from matplotlib import pyplot as pl                                              
import matplotlib                                                                
                                                                                 
def colours(index):                                                                                                                                                                                                                                                             
    cList = np.array([mpl.colors.hex2color(item['color']) for item in list(mpl.rcParams['axes.prop_cycle'])])
                                                                                    
    return cList[index]                                                             
                                                                                    
def colours_light(index):
    cList = np.array([mpl.colors.hex2color(item['color']) for item in list(mpl.rcParams['axes.prop_cycle'])])
    cListLight = np.array([mpl.colors.rgb_to_hsv(cc) for cc in cList])              
    cListLight[:,1] /= 2                                                            
    cListLight = np.array([mpl.colors.hsv_to_rgb(cc) for cc in cListLight])         
    return cListLight[index] 

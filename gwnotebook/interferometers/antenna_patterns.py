import numpy as np


# See Jarankowski Book on gravitational wave date analyisis or Satyhaprakash
# and Schutz review
# L-Shaped time independent antenna pattern no inclination
def antenna_pattern_L(theta, phi, polarization):
    f_plus = (0.5) * (1 + np.cos(theta)**2) * np.cos(2 * phi) * np.cos(2 * polarization) - \
        np.cos(theta) * np.sin(2 * phi) * np.sin(2 * polarization)

    f_cross = (0.5) * (1 + np.cos(theta)**2) * np.cos(2 * phi) * np.sin(2 * polarization) + \
        np.cos(theta) * np.sin(2 * phi) * np.cos(2 * polarization)

    return f_plus, f_cross


# L-Shaped  time independent antenna pattern including iota, i.e.
# the inclination angle between the source and the line of sight
def antenna_pattern_iota(theta, phi, polarization, iota):
    f_plus = (0.5) * (1 + np.cos(theta)**2) * np.cos(2 * phi) * np.cos(2 * polarization) - \
        np.cos(theta) * np.sin(2 * phi) * np.sin(2 * polarization)

    f_cross = (0.5) * (1 + np.cos(theta)**2) * np.cos(2 * phi) * np.sin(2 * polarization) + \
        np.cos(theta) * np.sin(2 * phi) * np.cos(2 * polarization)

    f_plus_iota = f_plus * ((1 + np.cos(iota)**2) / 2.0)

    f_cross_iota = f_cross * np.cos(iota)

    return f_plus_iota, f_cross_iota


# L-Shaped time independent total antenna pattern, i.e. square summing the two
# component Plus and Cross polarisation.
def antenna_pattern_total_iota(theta, phi, polarization, iota):
    f_p_iota, f_c_iota = antenna_pattern_iota(theta, phi, polarization, iota)

    F_total_iota = np.sqrt(f_p_iota**2 + f_c_iota**2)
    return F_total_iota
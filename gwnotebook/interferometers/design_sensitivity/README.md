# Design Strain Sensitivities (fequency [Hz] vs ASD [1/sqrt(Hz)])
crosscheck with [https://git.ligo.org/lscsoft/lalsuite/-/tree/master/lalsimulation/lib](https://git.ligo.org/lscsoft/lalsuite/-/tree/master/lalsimulation/lib)

## aLIGO (Advanced LIGO)
[https://dcc.ligo.org/LIGO-T1800044](https://dcc.ligo.org/LIGO-T1800044)

## aVirgo (Advanced Virgo)

# KAGRA


# Cosmic Explorer

# aPLUS (A+)
[https://dcc.ligo.org/LIGO-T1800042](https://dcc.ligo.org/LIGO-T1800042)

# ET
ET-D
[https://dcc.ligo.org/LIGO-P1600143](https://dcc.ligo.org/LIGO-P1600143)

# LISA

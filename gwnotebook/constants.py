import lal
import astropy.constants

# A list of constants within LAL, updated from the CODATA, can be found at
# https://lscsoft.docs.ligo.org/lalsuite/lal/_l_a_l_constants_8h.html
# or from astropy https://docs.astropy.org/en/stable/constants/


c_SI = lal.C_SI
G_SI = lal.G_SI
H0_SI = lal.H0_SI
Msun_SI = lal.MSUN_SI

# canonical moemnt of inertia [km * m**2]
I38 = 1e+38

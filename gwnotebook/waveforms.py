import numpy as np
import gwnotebook
import pycbc
from gwnotebook.constants import *
from gwnotebook.conversions import *


# TO DO: add all parameters in the waveform arguments
# https://pycbc.org/pycbc/latest/html/waveform.html

# TO DO:  match descriptive names as input parameters

def get_waveform_fd(Approximant='IMRPhenomD', Mass1=2, Mass2=2,
                    spin1_z=0.5, spin2_z=0,
                    Inclination=0,
                    Distance=1e3,
                    df=0.025, f_low=20, f_high=1000):

    from pycbc import waveform as wf

    htransform_p, htransform_c = wf.get_fd_waveform(
        approximant=Approximant,
        mass1=Mass1,
        mass2=Mass2,
        spin1z=spin1_z,
        spin2z=spin2_z,
        distance=Distance,
        delta_f=df,
        f_lower=f_low,
        f_final=f_high,  # if zero, leaves the choice to the model approximant used
        inclination=Inclination
    )
    # taking only value higher than 1 hz to avoid generation problems at low frequencies
    frqp = np.array(htransform_p.sample_frequencies)
    cut = 1
    htransform_p = np.array(htransform_p.data)[frqp >= cut]
    frqp = frqp[frqp >= cut]

    frqc = np.array(htransform_c.sample_frequencies)
    htransform_c = np.array(htransform_c.data)[frqc >= cut]
    frqc = frqc[frqc >= cut]
    return frqp, htransform_p, htransform_c


# time domain waveform
def get_waveform_td(Approximant='IMRPhenomD', Mass1=2, Mass2=2,
                    spin1_z=0.5, spin2_z=0,
                    Inclination=0,
                    Distance=1e3,
                    dt=0.025, f_low=20):

    from pycbc import waveform as wf

    hp, hc = wf.get_td_waveform(
        approximant=Approximant,
        mass1=Mass1,
        mass2=Mass2,
        spin1z=spin1_z,
        spin2z=spin2_z,
        distance=Distance,
        delta_t=dt,
        f_lower=f_low,  # if zero, leaves the choice to the md
        inclination=Inclination
    )
    # from object to array
    h_c = np.array(hc.data)
    h_p = np.array(hp.data)
    times = np.array(hp.sample_times)
    return times, h_p, h_c

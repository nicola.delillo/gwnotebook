
"""
GWnotebook
=====

This package contains naked modules to manipulate various gravitational wave
data type in form of posterior samples, timeseries or frequency domain series.
It also provides some basic tools for Compact Binary Coalescence waveforms
as well as signal processing utilities within the LIGO gravitational wave
data analysis framework

The aim is to provide routine tools for the daily python notebook use.


The code is hosted at https://git.ligo.org/nicola.delillo/gwnotebook .
For installation instructions see
https://git.ligo.org/nicola.delillo/gwnotebook/README.md


Contact: Nicola De Lillo <nicola.delillo@ligo.org>
"""

"""
[gwnotebook/]

* conversion.py contains a series of useful fucntion to manipulate with detector
noise curves, time and frequency domain waveform, or gravitational wave
physical parameters.

[gwnobebook/detetcors/]

* contains the detector noise sensitivities curve you and references
them.

* antenna_patterns.py  contains the definition of the time independent antenna
patterns for L shape and triangulare ET-like detectors.
"""

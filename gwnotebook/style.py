from __future__ import division
import numpy as np
import matplotlib as mpl
from matplotlib import pyplot as pl
import matplotlib


# [Styles]


# [Document text width (use \the\textwidth in LaTeX to get]

# latex rescale from Sean Leavy https://github.com/SeanDS/thesis/blob/master/scripts/lookfeel.py 

DOC_TEXT_WIDTH = 418.25368 # pt

def latex_dimension(scale):
    inches_per_pt = 1 / 72.27 # pt to inches
    dimension_inches = DOC_TEXT_WIDTH * inches_per_pt * scale # width in inches

    return dimension_inches

def latex_dimensions(scaleA, scaleB=None):
    if scaleB is None:
        scaleB = scaleA

    return latex_dimension(scaleA), latex_dimension(scaleB)


###
# Useful quantities

# default fig size
FIG_SIZE_A = latex_dimensions(1, 0.75)

# smaller fig size (shrunken height)
FIG_SIZE_A_SM = latex_dimensions(1, 0.5)

# faller fig size (extra height)
FIG_SIZE_A_TALL = latex_dimensions(1, 1)

# narrower figure (to fit an outer legend)
FIG_SIZE_B = latex_dimensions(0.75, 0.75)

# big figure for full page
FIG_SIZE_D = latex_dimensions(1, 1.4)

# fig sieze for notebok
FIG_SIZE_NB = (8,6)

# default line transparency
ALPHA_LINE_A = 0.8

# arrow widths
ARROW_WIDTH = 0.02
ARROW_HEAD_WIDTH = 0.1
ARROW_HEAD_LENGTH = 0.5
ARROW_OVERHANG = 0.3

# line widths
LW_NORMAL = 1.5
LW_THICK = LW_NORMAL * 2



# Matplotlib settings
import matplotlib.style


axes_linewidth = 1.75
legend_fontsize = 10
label_fontsize = 18
tick_label_fontsize = label_fontsize - 4
tick_linewidth = axes_linewidth - 0.5
tick_major_length = 7
tick_minor_length = tick_major_length  - 2.25
title_fontsize = label_fontsize
grid_linewidth = tick_linewidth
# https://matplotlib.org/tutorials/introductory/customizing.html
default_settings = {
    #'pgf.texsystem': 'pdflatex',
    #'pgf.rcfonts': False,
   # 'pgf.preamble': [
    #    r'\usepackage[utf8x]{inputenc}',
     #   r'\usepackage[T1]{fontenc}',
      #  r'\usepackage{siunitx}'
   # ],
   # 'text.usetex': True,
    'figure.figsize': FIG_SIZE_NB,
    'axes.titlesize': title_fontsize,
    'font.serif': [],
    'font.sans-serif': [],
    'font.monospace': [],
    #'font.family': 'sans-serif',
    'font.size': label_fontsize, #original 12
    'axes.labelsize': label_fontsize,
    'axes.linewidth': axes_linewidth,
    #'grid.alpha.major': 0.7,
    #'grid.alpha.minor': 0.2,
    'axes.grid': True,
    'grid.alpha': 0.7,
    'axes.grid.axis': 'both',
    'axes.grid.which': 'both',
    'grid.linestyle': '-',
    'grid.linewidth': grid_linewidth,
    'legend.borderaxespad': 1,
    'legend.fancybox': True,
    'legend.fontsize': legend_fontsize,
    'legend.framealpha': 1,
    'lines.linewidth': LW_NORMAL,
    'lines.markeredgewidth': 2,
    'lines.markersize': 8,
    'ytick.minor.left' : True,
    'xtick.top' : True,
    'xtick.minor.top' : True,
    'ytick.right': True,
    'ytick.minor.right' : True,
    'xtick.direction': 'in',
    'ytick.direction': 'in',
    'xtick.minor.visible': True,
    'ytick.minor.visible': True,
    'xtick.major.pad': 10,
    'xtick.minor.pad': 10,
    'xtick.major.size': tick_major_length,
    'xtick.minor.size': tick_minor_length,
    'xtick.major.width': tick_linewidth,
    'xtick.minor.width': tick_linewidth,
    'xtick.labelsize': tick_label_fontsize,
    'ytick.major.pad': 10, # padding betwween frame and tick label
    'ytick.minor.pad': 10,
    'ytick.major.size': tick_major_length,
    'ytick.minor.size': tick_minor_length,
    'ytick.major.width': tick_linewidth,
    'ytick.minor.width': tick_linewidth,
    'ytick.labelsize': tick_label_fontsize
}


#mpl.rcParams['axes.titlesize'] = 20
#mpl.rcParams['axes.labelsize'] = 26
#mpl.rcParams['xtick.top'] = True
#mpl.rcParams['ytick.right'] = True

# set grid styles not appearing in globa rcparams

def use_grid_style(ax):
    ax.grid(which='major', alpha=0.7)
    ax.grid(which='minor', alpha=0.15)

# set defaults
matplotlib.rcParams.update(default_settings)



def scale_fonts(factor):
    factor = float(factor)
  
    """Scales fonts by factor, for instance to handle LaTeX subplots"""
    matplotlib.rcParams.update({
	'font.size': default_settings['font.size'] * factor,
	'legend.fontsize': default_settings['legend.fontsize'] * factor,
	'xtick.labelsize': default_settings['xtick.labelsize'] * factor,
	'ytick.labelsize': default_settings['ytick.labelsize'] * factor
    })

def scale_markers(factor):
    factor = float(factor)
    
    """Scales markers, lines, etc. by factor"""
    matplotlib.rcParams.update({
	'lines.linewidth': default_settings['lines.linewidth'] * factor,
	'lines.markeredgewidth': default_settings['lines.markeredgewidth'] * factor,
	'lines.markersize': default_settings['lines.markersize'] * factor
    })

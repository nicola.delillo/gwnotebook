import numpy as np
import pesummary
import gwnotebook

def gwnotebook_path():
    """
    Return a string with the location of gwnotebook installation
    """

    # grab the path of the __init__.py file
    init_path = gwnotebook.__file__

    # now grab only the location of the ~/gwnotebok/
    return init_path.split('__init__.py')[0]
    

def sorted_magic( l ):
    """ Sorts the given iterable in the way that is expected.
 
    Required arguments:
    l -- The iterable to be sorted.
 
    """
    import re
    convert = lambda text: int(text) if text.isdigit() else text
    alphanum_key = lambda key: [convert(c) for c in re.split('([0-9]+)', key)]
    return sorted(l, key = alphanum_key)

def credible_interval(samples, perc_lower, perc_upper):                          
    '''                                                                          
    Return  left and right width of the credible interval of the samples,           
    specified by the user  (median + uppper/-lower)                                                      
      
    INPUT                                                                        
    -----                                                                        
    samples: list                                                                
    perc_lower: float                                                            
        the lower percentile                                                     
    perc_upper: float                                                            
        the upper percentile                                                     
    USAGE                                                                        
    -----                                                                        
    credible_interval(samples, 5, 95)                                            
    '''                                                                          
    median = np.median(samples)                                                  
    upper = np.percentile(samples, perc_upper) - median                          
    lower = median - np.percentile(samples, perc_lower)                          
 
    return median, lower, upper                                                  

    
def value_at_percentile(samples, perc_lower, perc_upper):                        
    '''                                                                          
    Return values of samples at median and percentiles specified                 
                                                                            
    INPUT                                                                        
    -----                                                                        
    samples: list                                                                
    perc_lower: float                                                            
        the lower percentile                                                     
    perc_upper: float                                                            
        the upper percentile                                                     
    USAGE                                                                        
    -----                                                                        
    value_at_percentile(samples, 5, 95)                                          
    '''                                                                          
    median = np.median(samples)                                                  
    upper = np.percentile(samples, perc_upper)                                   
    lower = np.percentile(samples, perc_lower)                                   
                                                                            
    return median, lower, upper  

def percentage_uncertainty(samples, percentage):                                 
    """                                                                          
    INPUT                                                                        
    -----                                                                        
    samples: list
        posterior samples                                                        
    percentage: float                                                            
        the percentage in terms of credible interval at which to calculate the   
        uncertainty  i.e. 50, 68 or 90.                                          
    
    OUTPUT                                                                       
    ------                                                                       
    perc_unc: float                                                              
        percentage uncertainty                                                   
    """                                                                          
    perc_upper = 100 - percentage/2                                              
    perc_lower = percentage/2                                                    
    
    median, lower, upper = value_at_percentile(samples, perc_lower, perc_upper)  
    perc_unc  = 100* (upper-lower)/median                                        
  
    return perc_unc                                                              

  
def fractional_uncertainty(samples, percentage):
    """
    INPUT
    -----
    samples: list
        posterior samples
    percentage: float
        the percentage in terms of credible interval at which to calculate the
        uncertainty  i.e. 50, 68 or 90

    OUTPUT
    ------
    frac_unc: float
        fractional uncertainty
    """
    perc_upper = 100 - percentage/2
    perc_lower = percentage/2
    
    median, lower, upper = value_at_percentile(samples, perc_lower, perc_upper)  
    frac_unc  =  (upper-lower)/median
    
    return frac_unc

def read_log_factors(path,  code):
    """
    Returns bayes factor and evidences for noise and signal. Accepts
    the path of the hdf5 or json file. Need to specify if lalinference
    or pbilby have been used
    
    INPUT
    -----
    path: str
        hdf5 or json file result
    code: str
        "lalinference" or "bibly" tag
        
    """
    import h5py
    from gwnotebook.read import read_json
    
    if code == 'lalinference' :
        f = h5py.File(path, "r")
        # logBF signal vs noise
        logBF = f['lalinference/lalinference_nest'].attrs['log_bayes_factor']
        # signal evidence
        logZ = f['lalinference/lalinference_nest'].attrs['log_evidence']
        logZnoise = f['lalinference/lalinference_nest'].attrs['log_noise_evidence']
        
        return {'log_evidence': logZ, 'log_noise_evidence': logZnoise, 'log_bayes_factor': logBF }
        
    elif code == 'bilby' : #if bilby or pbilby
        data = read_json(path)
        logZ = data['log_evidence']
        logZ_err = data['log_evidence_err']
        logZnoise = data['log_noise_evidence']
        logBF = data['log_bayes_factor']
        
        return {'log_evidence': logZ,'log_evidence_err': logZ_err, 'log_noise_evidence' : logZnoise, 'log_bayes_factor': logBF}
    


###############################################################################
#                   Saving files                                              #
##############################################################################
import pickle

# write python dict to a file
def write_pickle(path, dictionary):
    output = open(path, 'wb')
    pickle.dump(dictionary, output)
    output.close()
    return None

def read_pickle(file_path):
    pkl_file = open(file_path, 'rb')
    out_dictionary = pickle.load(pkl_file)
    pkl_file.close()
    return out_dictionary

###############################################################################
#                   Arrays manipulations                                        #
##############################################################################


def f_tuple(list1, list2, list3):
        """  
        
        INPUT
        ----------
        two 1-D numpy array or list
            
        
        RETURN
        ----------
        an array with all the combination of the elements of the two imput vector
       
        f_tuple(nparray([1,2,3]), np.array([1,2,3]))
        out: array([[1, 1], [1, 2], [1, 3], [2, 1], [2, 2], [2, 3],[3, 1],[3, 2], [3, 3]])
        
        """
        return np.array(list(product(list1, list2, list3)))
        
        
from scipy.stats import gaussian_kde


###############################################################################
#                K-L divergence                                              #
##############################################################################
def kl_divergence_kde(poserior_1, posterior_2, my_bw_method = None):
    '''
    Calculates the k-l divergence between two 1-D posterior samples, using a kde estimation.
    This will also work for multi-dimensional posteriors.
    
    INPUT
    -----
    poserior_1, posterior_2: array
        the two posterio samples to be compared
    my_bw_method: stror float
        option to use to determine the bandwidht (see documentation)
    '''
    
    # SMALL_CONSTANT is just a small constant (1e-50) which prevent the return of NaNs when dividing
    SMALL_CONSTANT = 1e-50
    p = gaussian_kde(poserior_1,bw_method=my_bw_method)#'scott') # 7.5e0 works best 
    q = gaussian_kde(posterior_2,bw_method=my_bw_method)#'scott')#'silverman') # 7.5e0 works best 
    
    
    log_diff = np.log((p(poserior_1)+SMALL_CONSTANT)/(q(poserior_1)+SMALL_CONSTANT))
    
    # Compute KL
    # set1.shape[1] is the length of samples 
    kl_result = (1.0/float(poserior_1.shape[0])) * np.sum(log_diff)
    # Compute symmetric kl
    
    anti_log_diff = np.log((q(posterior_2)+SMALL_CONSTANT)/(p(posterior_2)+SMALL_CONSTANT))
    anti_kl_result = (1.0/float(poserior_1.shape[0])) * np.sum(anti_log_diff)
    # if symmetric not wanted, don't add anti_kl_result variable
    kl_result = kl_result + anti_kl_result
    
    return kl_result
    


def kl_divergence_samples(poserior_1, posterior_2):
    '''
    Calculates the k-l divergence between two 2D posterior samples. It does
    not work for 1-D.
    Uses a python module for estimating divergence of two sets of samples generated
    from the two underlying distributions.
    
    The module can be found at https://github.com/slaypni/universal-divergence 
    
    INPUT
    -----
    poserior_1, posterior_2: 2D-array
        the two posterio samples to be compared

    '''
    from universal_divergence import estimate

    kl_result = estimate(poserior_1, posterior_2)
    return kl_result
  
 
# Downsampling
#define a function which downsamples posteriors uing np.random.choice - see pesummary
# one can apply np.random.choice it for a multidimensional posterior but
# posteriors  samples in  the raw - samples referred to the same prior volume sample - have to be always paired
# if one wants to use pesummary is load in with pesummary.io.read
#and then do f.downsample(n=10)
# then then f.to_dat(filename="hello")
# will produce the downsampled version


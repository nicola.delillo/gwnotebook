import numpy as np
import pesummary
from gwnotebook.conversions import asd_to_psd
from gwnotebook.utils import gwnotebook_path

# =============== [Reading noise spectral densities] ============== #
"""
Reads the core amplitude or power spectral density file .txt

RETURN
======
asd: numpy array
    two column: frequency in Hz, amplitude spectral density [1/sqrt(Hz)] or power
    spectral density [1/Hz]
"""


def aLIGO_asd(length=None, flow=None , df=None):

    # grab gwnotebook installation path
    path = gwnotebook_path() 

    # if user specifies a frequency range and step, interpolate amd return
    if (flow == None and length == None and df == None):
         #return the wholASD-ET_D_design_LIGO-P1600143.txte data set
        asd = np.loadtxt(path + 'interferometers/design_sensitivity/ASD-aLIGO_design_LIGO-T1800044.txt')
    else:
    # return the interpolation
        asd = np.loadtxt(path + 'interferometers/design_sensitivity/ASD-aLIGO_design_LIGO-T1800044.txt')
        fhigh = flow + (length-1) * df
        frequencies = np.linspace(flow,fhigh, length, endpoint=True)
        asd_interp = np.interp(frequencies, asd[:,0], asd[:,1])
        asd = np.stack((frequencies,asd_interp), axis=-1)
    
    return asd


def aLIGO_psd(length=None, flow=None , df=None):
    asd = aLIGO_asd(length, flow, df)
    psd = asd_to_psd(asd)
    return psd


def aPLUS_asd():
    path = gwnotebook_path()
    asd = np.loadtxt(path + 'interferometers/design_sensitivity/ASD-aPLUS_design_LIGO-T1800042.txt')
    return asd


def aPLUS_psd():
    path = gwnotebook_path()
    asd = np.loadtxt(path + 'interferometers/design_sensitivity/ASD-aPLUS_design_LIGO-T1800042.txt')
    psd = asd_to_psd(asd)
    return psd

def ET_asd(length=None, flow=None , df=None):

    # grab gwnotebook installation path
    path = gwnotebook_path()

    # if user specifies a frequency range and step, interpolate amd return
    if (flow == None and length == None and df == None):
         #return the whole data set
        asd = np.loadtxt(path + 'interferometers/design_sensitivity/ASD-ET_D_design_LIGO-P1600143.txt')
    else:
    # return the interpolation
        asd = np.loadtxt(path + 'interferometers/design_sensitivity/ASD-ET_D_design_LIGO-P1600143.txt')
        fhigh = flow + (length-1) * df
        frequencies = np.linspace(flow,fhigh, length, endpoint=True)
        asd_interp = np.interp(frequencies, asd[:,0], asd[:,1])
        asd = np.stack((frequencies,asd_interp), axis=-1)

    return asd

def ET_psd(length=None, flow=None , df=None):
    asd = ET_asd(length, flow, df)
    psd = asd_to_psd(asd)
    return psd

def curve_at_freq(data, freq):
    '''
    Returns the value of data at the desired x. Designed for asd or psd.

    INPUT
    -----
    data: 2d list or array
        [frequency, psd] or [frequency, asd]
    freq: float
        desired frequency
    '''
    frequencies = data[:,0]
    y = data[:,1]
    y_interp = np.interp(freq, frequencies, y)

    return y_interp
    
    
# ==================== [Frames] ================== #


def read_frame(gwf_file, channel_name):
    # https://pycbc.org/pycbc/latest/html/pycbc.frame.html
    # https://pycbc.org/pycbc/latest/html/frame.html
    """
    Reads a .gwf frame file and returns an object containing the timeseries
    with different attributes to check at https://pycbc.org/pycbc/latest/html/pycbc.types.html

    INPUT
    -----
    gw_file: .gwf
        frame file used for storing detector data
    channel_name: str
        name of the channel you want to load

    RETURN
    ------
    an object which contains the timeseries and the associated attributes to
    check at https://pycbc.org/pycbc/latest/html/pycbc.types.html

    EXAMPLES
    --------
    data = read_frame(gwf_file, channel_name[0])
    data_timeseries = data.numpy() #grabbing timeseries h(t) values
    data_times = data.get_sample_times().numpy() #grabbing time vector
    data_start_time= data.start_time #start time in gps time
    data_end_time= data.get_end_time() #end time in gps time
    data_duration = data.get_duration() #duration in secs
    data_sampling_rate = data.get_sample_rate() #sampling rate in Hz
    data_delta_t = data.get_delta_t() #time resolution
    data.delta_f #Return the delta_f this ts would have in the frequency domain
    The pycbc frame module provides methods for reading these files into TimeSeries objects
    as follows
    """
    from pycbc import frame

    return frame.read_frame(gwf_file, channel_name)


def get_channel_names(gwf_file):

    import gwpy
    from gwpy.timeseries import TimeSeriesDict
    """
    accept detector file .gwf and gives a list of the channel names in it
    """
    return gwpy.io.gwf.get_channel_names(gwf_file)

def get_gwf(channel, trigger_time, duration, file_name, padding_end=2.1, padding_start=0.1):
    """
    write a .gwf frame file containing the timeseries based on the provided
    trigger time. It will be written from
    start_time = trigger time - duration -padding_start
    end_time =  triiger_time + padding_start
    
    INPUT
    -----
    channel: string
        detector channel used (e.g: 'V1:Hrec_hoft_16384Hz or 'L1:GDS-CALIB_STRAIN_CLEAN')
    detectors: list
        list of detectors used
    trigger_time: float
        the trigger time of the event in GPS format
    duration: float
        duration of the time series in seconds
    file_name: string
        name to display: e.g. name.gwf
    padding_end: float
        seconds to pad to the end of the data . Default: 2.1.
    padding_start: float
        seconds to pad at the beginning of the data. Default 0.1.
    
    
    OUTPUT
    ------    
    .gwf file containing the time series
    """
    from gwpy.timeseries import TimeSeries


    end_time = trigger_time + padding_end
    start_time = end_time - duration-padding_start

    data = TimeSeries.get(channel, start_time, end_time)
    
    return TimeSeries.write(data, target= file_name +'.gwf',format='gwf')


import numpy as np
import corner
from matplotlib import pyplot as plt
from gwnotebook.utils import credible_interval
from gwnotebook.colour import colours

default_corner_kwargs = dict(
        bins=50, smooth=0.9, label_kwargs=dict(fontsize=16),
        title_kwargs=dict(fontsize=16),
        truth_color='tab:orange', quantiles=[0.16, 0.84],
        levels=(1 - np.exp(-0.5), 1 - np.exp(-2), 1 - np.exp(-9 / 2.)),
        plot_density=True, plot_datapoints=True, max_n_ticks=3)

def hist1d(samples,  xlabel, labels, prior = np.array([None]) , bins=50, xlim = None, density=True):
    '''
    Return a 1D histogram.

    INPUT
    -----
    bins: int
        number of bins
    samples:
        Can be the list of smaples from one parameter coming one or more PE runs.
    prior:
        supposed to be the same for the two runs
    xlabel: str
        name of the parameter
    labels:
        the label or labels for differnt pe run
    density: bool
        normalise to 1 the area of the histogram if True (Default)
    USAGE:
    ------
    for single run: hist1d(samples_1['m1'], xlabel='m1 [Msun]', labels='exp1', prior = prior_m1)
    for multipe runs: hist1d([samples_1['m1'], samples_2], xlabel='m1 [Msun]', labels=['exp1', 'exp2'], prior = prior_m1)
    '''
    # grabbing the shape of the vectore to spot how many pe runs are present
    shape = len(np.array(samples))
    prior=np.array(prior)
    width_lines = 2.5
    width_priors = 1.5
    title_size = 18
    xlabel_size = 16
    # checking how many samples from different runs for this paramaters
    # only one parameter estimation job, no prior
    # if shape > 100  -hardly imagine one would compare more than 20 runs and
    # having less tehn 20 samples-
    if (shape > 20 and prior.any() == None) :
        # calculating credible intervals
        median, lower, upper = credible_interval(samples, 5, 95)

        plt.hist(samples, bins=bins, histtype=u'step', edgecolor=colours(0), label=labels, lw=width_lines, density=density)
        plt.title(r"$%s^{+%s}_{-%s}$" % (median, upper, lower), fontsize=title_size, pad=10)
        plt.ylabel(None)
        plt.xlabel(xlabel, fontsize=xlabel_size)
        plt.legend()
       # if prior is not None, print also that
    if(shape > 20 and prior.any() != None)  :
        # calculating credible interval
        median, lower, upper = credible_interval(samples, 5, 95)

        plt.hist(samples, bins=bins, histtype=u'step', edgecolor=colours(0), label=labels, lw=width_lines, density=density)
        plt.hist(prior, bins=bins,histtype=u'step',edgecolor='k', label='prior' , lw=width_priors, linestyle='--', density=density)
        plt.title(r"$%s^{+%s}_{-%s}$" % (median, upper, lower), fontsize=title_size, pad=10)
        plt.ylabel(None)
        plt.xlabel(xlabel, fontsize=xlabel_size)
        plt.legend()

    # if more than one list of  samples for the same parameter from differnt pe jobs

    if (shape < 20 and prior.any() == None) :
        for i in np.arange(len(samples)):
            plt.hist(samples[i], bins=bins, histtype=u'step', edgecolor=colours(i), label=labels[i], lw=width_lines, density=density)
        plt.ylabel(None)
        plt.xlabel(xlabel,fontsize=xlabel_size)
        plt.legend()

    # plot the samples for the same parameter from differnt pe jobs. Priors are supposed to be the same
    if (shape < 20 and prior.any() != None) :
        for i in np.arange(shape):
            plt.hist(samples[i], bins=bins, histtype=u'step', edgecolor=colours(i), label=labels[i], lw=width_lines, density=density)
        plt.hist(prior, bins=bins,histtype=u'step',edgecolor='k', label='prior' , lw=width_priors, linestyle='--', density=density)
        plt.ylabel(None)
        plt.xlabel(xlabel, fontsize=xlabel_size)
        plt.legend()

    return None


def corner_comparison(samples, other_samples=None, reweight=False, density=True,
        truth=None, labels=None, **kwargs):
    """
    Return a corner plot with two set of data plotted

    Parameters
    ----------
    samples: array_like
        If other_samples is None then array must be (2, # samples, # dims)
        else (# sample, # dims)
    other_samples: array_like
        Samples that are compared to base samples
    reweight: bool
        Reweight other samples by length of samples
    density: bool
        Plot density
    truth: array_like
        If not None, truth will be plotted on the figure
    labels: array_like
        List of strings to use as axis labels
    kwargs:
        kwargs passed to coner.corner

    Return
    ------
    Instance of matplotlib.pyplot figure
    """
    corner_kwargs = default_corner_kwargs.copy()
    corner_kwargs.update(kwargs)

    if other_samples is not None:
        base_samples = samples
    elif len(samples) == 2:
        base_samples = samples[0]
        other_samples = samples[1]
    else:
        raise RuntimeError('Must provide two sets of samples to compare!')

    if samples.shape[-1] < 2:
        raise RuntimeError('Input is one dimensional')

    if truth is not None:
        corner_kwargs['truths'] = truth

    if not density:
        corner_kwargs['plot_density'] = False

    if labels is not None:
        corner_kwargs['labels'] = labels

    # ranges so that histograms are plotted with same bins
    max_v = np.max([np.max(p, axis=0) for p in [base_samples, other_samples]], axis=0)
    min_v = np.min([np.min(p, axis=0) for p in [base_samples, other_samples]], axis=0)
    ranges = np.array([min_v, max_v]).T

    base_fig = corner.corner(base_samples, color='tab:blue', fill_contours=True,
            range=ranges, density=density, **corner_kwargs)

    # weight so histogram bins heights match if there are different number
    # of samples
    if reweight:
        weights = np.ones(len(other_samples)) * (len(base_samples) / len(other_samples))
    else:
        weights = None

    combined_fig = corner.corner(other_samples, fig=base_fig, color='tab:red',
            fill_contours=True, weights=weights, range=ranges, density=density,
            **corner_kwargs)

    return combined_fig



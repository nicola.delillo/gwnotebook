def read_xml(filename):
    """
    this reads the .xml.gz metafile from lalinference and prints it as a table
    useful in case of injections
    """

    from gwpy.table import EventTable
    return EventTable.read(filename, tablename='sim_inspiral')


def read(path):

    from pesummary.gw.file.read import read as rd
    """Read in a results file.

    Parameters
    ----------
    path: str
        path to results file
    """
    return rd(path)

def read_json(path):
    import json
    with open(path, "r") as f:
        data = json.load(f)
    f.close
    return data


def make_sample_dict_from_pbilby(file_json):
    dictionary = {}
    for item in file_json['posterior']['content'].keys():
        # for each index of the posterior sample, read samples and save it as key in a dictionar
        dictionary[item] = file_json['posterior']['content'][item]
        
    return dictionary